#ifndef interface_types_h
#define interface_types_h

namespace ipc = boost::interprocess;

namespace constants {
  const char* MESSAGE_QUEUE_NAME = "cos_generator_client";
  const size_t MESSAGE_QUEUE_SIZE = 16;
  const uint8_t TIMEOUT = 5;
}

namespace model {
  typedef double data_unit;
  const double MAX_SPEED = 250.0;
}
#endif