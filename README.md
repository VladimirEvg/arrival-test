# ARRIVAL TEST

Just a small server client application.

## Requirements

CMake 3.15.4, Boost 1.70

## Installation

```bash
version=3.15

build=4

mkdir ~/temp

cd ~/temp

wget https://cmake.org/files/v$version/cmake-$version.$build-Linux-x86_64.sh 

sudo mkdir /opt/cmake

sudo sh cmake-$version.$build-Linux-x86_64.sh --prefix=/opt/cmake

sudo ln -s /opt/cmake/bin/cmake /usr/local/bin/cmake

sudo apt-get install build-essential g++ python-dev autotools-dev libicu-dev build-essential libbz2-dev libboost-all-dev

wget -O boost_1_70_0.tar.gz https://sourceforge.net/projects/boost/files/boost/1.70.0/boost_1_70_0.tar.gz/download

tar xzvf boost_1_70_0.tar.gz

cd boost_1_70_0/

.\bootstrap

.\b2

```

## Usage

Just run both applications