//basic
#include <iostream>
#include <memory>
#include <mutex>
#include <thread>
#include <condition_variable>
#include <csignal>

//boost
#include <boost/interprocess/ipc/message_queue.hpp>
#include <boost/date_time.hpp>

//internal
#include "../common/interface_types.hpp"
#include "message_queue_client.hpp"

//helpers
std::atomic<bool> termination_signal = false;

//sync
std::mutex mutex;
std::condition_variable speed_value_cv;
std::atomic<bool> new_speed_value_recieved  = { false };
std::atomic<bool> new_speed_value_displayed = { false };

//data
model::data_unit data = 0.0;

void signal_handler(int signal) { 
  termination_signal = true;
}

void speed_display_func(const model::data_unit& value) {
    do {
        std::unique_lock<std::mutex> lock(mutex);
        speed_value_cv.wait(lock, [] { return true == new_speed_value_recieved; });
        
        std::cout << value << std::endl;
        
        new_speed_value_recieved = false;
        new_speed_value_displayed = true;
        
        lock.unlock();
        speed_value_cv.notify_one();
    } while (!termination_signal);
}

void speed_receive_func(const char* queue_name) {
    unsigned int priority;
    ipc::message_queue::size_type size;
    auto continue_recieving = true;
    model::data_unit result;
    clients::message_queue_client<model::data_unit> client (queue_name); 
    do {
            {
                data = client.timed_receive(constants::TIMEOUT);
                std::lock_guard<std::mutex> lock(mutex);
                new_speed_value_recieved = true;
                new_speed_value_displayed = false;
            }

            speed_value_cv.notify_one();

            {
                std::unique_lock<std::mutex> lock(mutex);
                speed_value_cv.wait(lock, []{ return true == new_speed_value_displayed; });
            }
    }
    while(!termination_signal && continue_recieving);
}

int main(int argc, const char * argv[]) {
    
    signal(SIGABRT, &signal_handler);
    signal(SIGTERM, &signal_handler);
  	signal(SIGINT, &signal_handler);

    std::thread speed_display_thread;
    std::thread speed_receive_thread;
    
    try {
        speed_display_thread = std::thread(&speed_display_func, std::ref(data));    
        speed_receive_thread = std::thread(&speed_receive_func, constants::MESSAGE_QUEUE_NAME);
    } catch(ipc::interprocess_exception &ex) {
        std::cerr << "Error on startup: " << ex.what() << std::endl;
        return 1;
    }

    speed_display_thread.join();
    speed_receive_thread.join();

    std::cout << "Timed out or terminated" << std::endl;

    return 0;
}