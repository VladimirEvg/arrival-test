#ifndef message_queue_client_h
#define message_queue_client_h

//boost
#include <boost/interprocess/ipc/message_queue.hpp>

//internal
#include "../common/interface_types.hpp"
#include <boost/interprocess/ipc/message_queue.hpp>


namespace clients {

  template<typename T>
  class message_queue_client {

  public:
    message_queue_client(const char* name ) 
      : mq_ { boost::interprocess::open_only, name }
    {}

    T timed_receive(uint8_t seconds_timeout) {
      T result;
      boost::interprocess::message_queue::size_type recvsize;
      unsigned int priority = 0;
      mq_.timed_receive (&result, sizeof(result), recvsize, priority, boost::posix_time::ptime(boost::posix_time::microsec_clock::universal_time()) + boost::posix_time::seconds(seconds_timeout));
      return result;
    }

  private:
    
    message_queue_client() = delete;
    message_queue_client(const message_queue_client&) = delete;
    message_queue_client(message_queue_client&& other) = delete;

    boost::interprocess::message_queue mq_;
  };
}
#endif