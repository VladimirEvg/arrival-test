//basic
#include <iostream>
#include <memory>
#include <future>
#include <cmath>
#include <csignal>

//boost
#include <boost/date_time.hpp>
#include <boost/interprocess/ipc/message_queue.hpp>
#include <boost/math/constants/constants.hpp>

//internal
#include "../common/interface_types.hpp"

//helpers
std::atomic<bool> termination_signal = false;
void signal_handler(int signal) { 
  termination_signal = true;
}

model::data_unit cos_from_max_speed () {
    auto duration = std::chrono::system_clock::now().time_since_epoch();
    auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
    auto cos = std::cos(millis * boost::math::constants::pi<model::data_unit>() / 180.0);
    return abs(model::MAX_SPEED * cos);
}

//generation thread func
void generator_func(std::shared_ptr<ipc::message_queue> pmq) {

  model::data_unit next_value = 0.0;
  auto procced_sending = true;
  do
  {
      if(constants::MESSAGE_QUEUE_SIZE == pmq->get_num_msg()) {
        std::cout << "warning: message queue size maximum" << std::endl;
      }
      
      std::this_thread::sleep_for(std::chrono::milliseconds(1));
      next_value = cos_from_max_speed();
      procced_sending = pmq->timed_send(&next_value
        , sizeof(model::data_unit)
        , 0
        , boost::posix_time::ptime(boost::posix_time::microsec_clock::universal_time()) + boost::posix_time::seconds(constants::TIMEOUT));
  }
  while(!termination_signal && procced_sending);

}

int main(int argc, const char * argv[]) {

    signal(SIGABRT, &signal_handler);
    signal(SIGTERM, &signal_handler);
  	signal(SIGINT, &signal_handler);

  	std::shared_ptr<ipc::message_queue> p_message_queue;
    try{
        ipc::message_queue::remove(constants::MESSAGE_QUEUE_NAME);
        p_message_queue = std::make_shared<ipc::message_queue>(ipc::create_only
                        , constants::MESSAGE_QUEUE_NAME
                        , constants::MESSAGE_QUEUE_SIZE
                        , sizeof(model::data_unit));
    }
    catch(ipc::interprocess_exception &ex) {
        std::cout << ex.what() << std::endl;
        return 1;
    }

    auto thread = std::thread(&generator_func, p_message_queue);

    std::cout << "server started" << std::endl;

    thread.join();

    std::cout << "Timout or terminated" << std::endl;

    return 0;
}